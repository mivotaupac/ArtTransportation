<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="main.entities.Box"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Lista de Cajas - <%=session.getAttribute("name_business")%></title>

</head>
<body>
	<h1>Listado de cajas</h1>
	<h4>Usuario: <%=session.getAttribute("name_user")%></h4>
	<br>
	<table>
		<%
			List<Box> boxes = (List<Box>) request.getAttribute("boxes");
			if (boxes.size() > 0) {
		%>
		<tr>
			<th width="200px">UUID caja</th>
			<th width="200px">enlace</th>
		</tr>
		<%
				for (Box box : boxes) {
		%>
		<tr align="center">
			<td><%=box.getUuid()%></td>
			<td><a href="boxes/box?uuid_box=<%=box.getUuid()%>"><img src='picture?code_qr=<%=box.getUuid()%>' alt="Smiley face" height="42" width="42"><%=box.getUuid()%></a></td>
		</tr>
		<%
				}
			} else {
		%>
		<tr>
			<td>No hay Cajas para mostrar</td>
		</tr>
		<%
			}
		%>
	</table>

</body>
</html>