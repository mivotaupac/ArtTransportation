<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<script type="text/javascript">
	var error_req =<%=request.getAttribute("error")%>;
	var count = 0;
	
	window.onload = function() {
		document.getElementById("error").innerHTML = error_req.error;
		document.getElementById("detail_error").innerHTML = error_req.detailError;
	}

	function hideErrors() {
		if (count <= 6) {
			count = count + 1;
		} else {
		    clearInterval(interval);
		    document.getElementById("div_error").style.visibility='hidden';
		}
		document.getElementById("reloj").innerHTML = "Redirigimos en: " + count
				+ " segundos";
	}

	var interval = setInterval(hideErrors, 1000);

	window.onload = function() {
		document.getElementById("error").innerHTML = error_req.error;
		document.getElementById("detail_error").innerHTML = error_req.detailError;
	}
</script>
</head>
<body>

<div id="div_error">
	<h2 id="error"></h2>
	<p id="detail_error"></p>
</div>

<form action="login" method="post">
<label>Usuario</label><input type="text" name="id"/>
<label>Password</label><input type="password" name="password"/>
<button type="submit">Logearse</button>
</form>

</body>
</html>