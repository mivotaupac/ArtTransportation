<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Error</title>
<script type="text/javascript">
	var error_req =<%=request.getAttribute("error")%>;
	var count = 5;

	function tiempoRestante() {
		if (count > 0) {
			count = count - 1;
		}
		document.getElementById("reloj").innerHTML = "Redirigimos en: " + count
				+ " segundos";
	}

	window.onload = function() {
		document.getElementById("error").innerHTML = error_req.error;
		document.getElementById("detail_error").innerHTML = error_req.detailError;

		setInterval(tiempoRestante, 1000);
	}
</script>
</head>
<body>
<div class="error">
	<h2 id="error"></h2>
	<p id="detail_error"></p>
</div>
<div class="aviso">
	<h5 id="reloj"></h5>
</div>
</body>
</html>