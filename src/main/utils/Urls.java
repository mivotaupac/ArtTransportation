package main.utils;

public class Urls {
	
	public static final String HTML_LOGIN = "/login";
	public static final String HTML_LOGOUT = "/logout";

	public static final String HTML_BOXES = "/boxes";
	public static final String HTML_CREATE_BOX = "/boxes/create-box";
	public static final String HTML_DETAIL_BOX = "/boxes/box";
	public static final String HTML_ADD_PICTURE = "/boxes/add-picture";

	public static final String HTML_PRINT_PICTURE = "/picture";
	
	private Urls() {

	}

}
