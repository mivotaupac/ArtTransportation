package main.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

import main.entities.Box;
import main.entities.Picture;
import main.persistence.dao.DAOBox;

public class CodeUtils {

	public static final String TYPE_ADVISOR = "ADVISOR";
	public static final String TYPE_ERROR = "ERROR";

	public String getJson(Object o) {
		Gson gson = new Gson();
		JsonObject json = new JsonObject();
		json.addProperty("result", true);
		json.add("data", gson.toJsonTree(o));
		return gson.toJson(json);
	}

	public String errorJson(String type, String error, String detailError) {
		Gson gson = new Gson();
		JsonObject json = new JsonObject();
		json.addProperty("result", false);
		json.addProperty("type", type);
		json.addProperty("error", error);
		json.addProperty("detailError", detailError);
		return gson.toJson(json);
	}

	public String generateUuid(int idBussine) {
		return String.format("%04d-%d-%06.0f", idBussine, System.currentTimeMillis(), Math.random() * 1000000);
	}

	public byte[] generateQR(String code, int width, int height) {

		byte[] qrCode = null;

		try {
			QRCodeWriter writer = new QRCodeWriter();

			BitMatrix matrix = writer.encode(code, BarcodeFormat.QR_CODE, width, height);

			BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
			image.createGraphics();

			Graphics2D graphics = (Graphics2D) image.getGraphics();
			graphics.setColor(Color.WHITE);
			graphics.fillRect(0, 0, width, height);
			graphics.setColor(Color.BLACK);

			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					if (matrix.get(i, j)) {
						graphics.fillRect(i, j, 1, 1);
					}
				}
			}

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(image, "jpg", baos);
			qrCode = baos.toByteArray();

		} catch (WriterException | IOException e) {
			Logger.getLogger(DAOBox.class.getName()).log(Level.SEVERE, null, e);
		}

		return qrCode;
	}
}
