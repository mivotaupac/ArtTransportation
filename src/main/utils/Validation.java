package main.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Validation {
	
	public static String generateHashSha256(String message) {
		
		String hash = null;
		
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.reset();
			byte[] buffer = message.getBytes("UTF-8");
			md.update(buffer);
			byte[] digest = md.digest();
			
			StringBuilder sb = new StringBuilder();
		    for (int i = 0; i < digest.length; i++) {
		        sb.append(Integer.toString((digest[i] & 0xff) + 0x100, 16).substring(1));
		    }
		    
		    hash = sb.toString();
			
		} catch (NoSuchAlgorithmException e) {
			Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, e);
		} catch (UnsupportedEncodingException e) {
			Logger.getLogger(Validation.class.getName()).log(Level.SEVERE, null, e);
		}

		return hash;
	}

}
