package main.entities;

import java.io.Serializable;

public class Box implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String uuid;
	private int idCompany;
	
	public String getUuid() {
		return uuid;
	}
	
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public int getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}

	@Override
	public String toString() {
		return "Box [uuid=" + uuid + ", idCompany=" + idCompany + "]";
	}
	
}
