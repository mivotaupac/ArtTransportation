package main.entities;

public class User {

	private String identification;
	private String name;
	private int idCompany;
	private boolean valid;
	
	public String getIdentification() {
		return identification;
	}
	
	public void setIdentification(String identification) {
		this.identification = identification;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public int getIdCompany() {
		return idCompany;
	}

	public void setIdCompany(int idCompany) {
		this.idCompany = idCompany;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public String toString() {
		return "User [identification=" + identification + ", name=" + name + ", idCompany=" + idCompany + ", valid=" + valid + "]";
	}
	
}
