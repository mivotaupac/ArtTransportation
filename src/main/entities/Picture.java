package main.entities;

public class Picture {
	
	private int id;
	private String name;
	private String uuidBox;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getUuidBox() {
		return uuidBox;
	}
	
	public void setUuidBox(String uuidBox) {
		this.uuidBox = uuidBox;
	}

	@Override
	public String toString() {
		return "Picture [id=" + id + ", name=" + name + ", uuidBox=" + uuidBox + "]";
	}
	
}
