package main.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import main.entities.User;
import main.persistence.dao.DAOUser;
import main.utils.CodeUtils;
import main.utils.Urls;
import main.utils.Validation;

/**
 * Servlet implementation class ServletLogin
 */
@WebServlet(name = "LoginServlet", description = "Servlet for login users", urlPatterns = { "/login" })
public class ServletLogin extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		//DBUtils.initDB();
		
		if (session != null) {
			session.invalidate();
		}
        
		request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);

        String identification=request.getParameter("id");
        String password=request.getParameter("password");

		CodeUtils codeUtils = new CodeUtils();

		if (identification != null && password != null) {

			DAOUser userDAO = new DAOUser();

			if (identification != null && userDAO.validatePass(identification, Validation.generateHashSha256(password))) {
				User user = userDAO.getUserById(identification);
				String name_business = userDAO.getBusinessName(user.getIdCompany());
				
				//Seteamos cuatro variables en la sesión, simplemente para no volver a cargarlos cada vez que se necesiten
				session.setAttribute("id_user", identification);
				session.setAttribute("name_user", user.getName());
				session.setAttribute("code_business", user.getIdCompany());
				session.setAttribute("name_business", name_business);
				response.sendRedirect(Urls.HTML_BOXES.replaceFirst("/", "") );
			} else {
				request.setAttribute("error",
						codeUtils.errorJson(CodeUtils.TYPE_ERROR, "Contraseña incorrecta", "Vuelva a intentarlo"));
				request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
			}

		} else {
			response.addHeader("REFRESH", "6; URL = " + Urls.HTML_BOXES.replaceFirst("/", ""));
			request.setAttribute("error", codeUtils.errorJson(CodeUtils.TYPE_ERROR,
					"Error " + HttpServletResponse.SC_BAD_REQUEST, "No se han indicado los parámetros correctos"));
			request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
		}
	}

}
