package main.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.persistence.dao.DAOBox;
import main.persistence.dao.DAOPicture;
import main.utils.Urls;

/**
 * Servlet implementation class ImageServlet
 */
@WebServlet({ Urls.HTML_PRINT_PICTURE })
public class ImageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {	
		String codeQR = request.getParameter("code_qr");
		String idPictureString = request.getParameter("id");
		
		if (codeQR != null) {
			DAOBox boxDAO = new DAOBox();
			byte[] file = boxDAO.getImageQR(codeQR);
			
			printImage(file, response);
			
		} else if (idPictureString != null) {
			int idPicture = Integer.getInteger(idPictureString);
			if (idPicture > 0) {
				DAOPicture pictureDAO = new DAOPicture();
				byte[] file = pictureDAO.getImagePicture(idPicture);
				
				printImage(file, response);
			}
		}
		
		//response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}

	private void printImage(byte[] file, HttpServletResponse response) throws IOException {
		response.setContentType("image/jpg");
		
		response.getOutputStream().write(file);
	}

}
