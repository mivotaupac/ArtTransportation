package main.servlets;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import main.entities.Box;
import main.entities.Picture;
import main.persistence.dao.DAOBox;
import main.persistence.dao.DAOPicture;
import main.utils.CodeUtils;
import main.utils.Urls;

/**
 * Servlet implementation class BoxesServlet
 */
@WebServlet({ Urls.HTML_BOXES, Urls.HTML_DETAIL_BOX, Urls.HTML_CREATE_BOX, Urls.HTML_ADD_PICTURE })
public class BoxesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(true);

		if (session == null) {
			response.sendRedirect(Urls.HTML_LOGIN);
		}

		String pageUrl = request.getServletPath();

		switch (pageUrl) {
		case Urls.HTML_BOXES:
			showListBoxes(session, request, response);
			break;
		case Urls.HTML_DETAIL_BOX:
			showBox(request, response);
			break;
		case Urls.HTML_CREATE_BOX:
			// si entra por get es que no se ha confirmado la creación de la
			// caja
			response.sendRedirect(Urls.HTML_BOXES);
			break;
		case Urls.HTML_ADD_PICTURE:
			// si entra por get es que no se ha confirmado la creación de la
			// pintura
			response.sendRedirect(Urls.HTML_BOXES);
			break;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession(false);

		if (session == null) {
			response.sendRedirect(Urls.HTML_LOGIN);
		}

		String pageUrl = request.getServletPath();

		switch (pageUrl) {
		case Urls.HTML_ADD_PICTURE:
			addNewPicture(session, request, response);
			break;
		case Urls.HTML_CREATE_BOX:
			createNewBox(session, request, response);
			break;
		}
	}

	private void showListBoxes(HttpSession session, HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		Integer codBusiness = (Integer) session.getAttribute("code_business");

		CodeUtils codeUtils = new CodeUtils();

		// Si el codigo de la empresa se ha pasado tiene que obtener la lista de
		// las cajas de base de datos
		if (codBusiness != null) {

			DAOBox boxDAO = new DAOBox();
			List<Box> boxes = boxDAO.getBoxesByCompany(codBusiness);

			// enviamos los datos de las cajas, en caso de no haber se envia un
			// mensaje de aviso
			if (boxes != null) {
				request.setAttribute("boxes", boxes);
			} else {
				request.setAttribute("boxes", codeUtils.errorJson(CodeUtils.TYPE_ADVISOR, "No se han encontrado cajas",
						"Sin elementos a mostrar"));
			}
			// cargamos la plantilla de jsp
			request.getRequestDispatcher("/WEB-INF/jsp/list_boxes.jsp").forward(request, response);
		} else {
			// este apartado creo que no tendría que controlarlo, pero por si
			// acaso... se da en el caso de que la sesion esta activa pero sin
			// cargarse las variables que se setean en el login (id y nombre de
			// empresa)
			// redireccionamos a los 6 segundos a la página de login
			response.addHeader("REFRESH", "6; URL = " + Urls.HTML_LOGIN.replaceFirst("/", ""));
			// mientras no se redirecciona a
			request.setAttribute("error",
					codeUtils.errorJson(CodeUtils.TYPE_ERROR, "Error de Sesion", "Faltan parámetros"));

			request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
		}

	}

	private void showBox(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		String codeBox = request.getParameter("uuid_box");

		CodeUtils codeUtils = new CodeUtils();

		if (codeBox != null) {

			DAOPicture pictureDAO = new DAOPicture();
			List<Picture> pictures = pictureDAO.getPicturesByBox(codeBox);

			if (pictures != null) {
				if (pictures.size() > 0) {
					request.setAttribute("pictures", codeUtils.getJson(pictures));
				} else {
					request.setAttribute("pictures", codeUtils.errorJson(CodeUtils.TYPE_ADVISOR,
							"No se han encontrado pinturas", "Sin elementos a mostrar"));
				}
				request.getRequestDispatcher("/WEB-INF/jsp/detail_box.jsp").forward(request, response);
			} else {
				// redireccionamos a la lista de cajas en el caso de que no se
				// pudiera recuperar la pintura de BD
				response.addHeader("REFRESH", "6; URL = " + Urls.HTML_BOXES.replaceFirst("/", ""));
				request.setAttribute("boxes",
						codeUtils.errorJson(CodeUtils.TYPE_ERROR, "Error BD", "Error al consultar en bases de datos"));
				request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
			}

		} else {
			response.addHeader("REFRESH", "6; URL = " + Urls.HTML_BOXES.replaceFirst("/", ""));
			// e puesto codigo de error SC_BAD_REQUEST por probar simplemente,
			// pero no se si iria aquí, como es el caso de que faltan parámetros
			// o se han pasado mal....
			request.setAttribute("error", codeUtils.errorJson(CodeUtils.TYPE_ERROR,
					"Error " + HttpServletResponse.SC_BAD_REQUEST, "No se han indicado los parámetros correctos"));
			request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
		}

	}

	private void createNewBox(HttpSession session, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Integer codBusiness = (Integer) session.getAttribute("code_business");

		CodeUtils codeUtils = new CodeUtils();

		if (codBusiness != null) {

			String uuid = codeUtils.generateUuid(codBusiness);
			byte[] codeQR = codeUtils.generateQR(uuid, 300, 300);

			Box box = new Box();
			box.setUuid(uuid);
			box.setIdCompany(codBusiness);

			DAOBox boxDAO = new DAOBox();

			if (boxDAO.insertBox(box, codeQR)) {
				// se redirecciona al detalle de la caja creada
				response.sendRedirect(Urls.HTML_DETAIL_BOX.replaceFirst("/", "") + "?uuid_box=" + uuid);
			} else {
				response.addHeader("REFRESH", "6; URL = " + Urls.HTML_BOXES.replaceFirst("/", ""));
				request.setAttribute("error",
						codeUtils.errorJson(CodeUtils.TYPE_ERROR, "Error BD", "Error al intentar crear la caja"));
				request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
			}

		} else {
			response.addHeader("REFRESH", "6; URL = " + Urls.HTML_LOGIN.replaceFirst("/", ""));
			request.setAttribute("error",
					codeUtils.errorJson(CodeUtils.TYPE_ERROR, "Error de Sesion", "Faltan parámetros"));
			request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
		}
	}

	private void addNewPicture(HttpSession session, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String uuid = request.getParameter("uuid_box");

		CodeUtils codeUtils = new CodeUtils();

		if (uuid != null) {

			DAOPicture pictureDAO = new DAOPicture();

			Picture picture = new Picture();
			String name = (String) request.getAttribute("name");
			byte[] image = obtenirFileImage(request.getPart("image"));

			picture.setId(pictureDAO.getNextSequence());
			picture.setName(name);

			if (name == null || pictureDAO.insertPicture(picture, image)) {
				response.sendRedirect(Urls.HTML_DETAIL_BOX.replaceFirst("/", "") + "?uuid_box=" + uuid);
			} else {
				response.addHeader("REFRESH", "6; URL = " + Urls.HTML_DETAIL_BOX.replaceFirst("/", ""));
				request.setAttribute("error",
						codeUtils.errorJson(CodeUtils.TYPE_ERROR, "Error BD", "Error al intentar crear la pintura"));
				request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
			}

		} else {
			response.addHeader("REFRESH", "6; URL = " + Urls.HTML_BOXES.replaceFirst("/", ""));
			request.setAttribute("error", codeUtils.errorJson(CodeUtils.TYPE_ERROR,
					"Error " + HttpServletResponse.SC_BAD_REQUEST, "No se han indicado los parámetros correctos"));

			request.getRequestDispatcher("/WEB-INF/jsp/error.jsp").forward(request, response);
		}
	}

	private byte[] obtenirFileImage(Part filePart) throws IOException {
		InputStream inputStream = filePart.getInputStream();
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		byte[] bytesTemporals = new byte[1024];
		while (inputStream.read(bytesTemporals) != -1) {
			outputStream.write(bytesTemporals);
		}
		return outputStream.toByteArray();
	}

}
