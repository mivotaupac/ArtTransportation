package main.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import main.utils.Validation;

public class DBUtils {

	public static synchronized Connection getConnection() {
		try {
			Context ic = new InitialContext();
			DataSource ds = (DataSource) ic.lookup("java:comp/env/jdbc/artDB");
			Connection con = ds.getConnection();
			return con;
		} catch (NamingException e) {
			Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
		} catch (SQLException e) {
			Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
		}
		return null;
	}

	public static void closeResources(AutoCloseable... resources) {
		for (AutoCloseable autoCloseable : resources) {
			if (autoCloseable != null) {
				try {
					autoCloseable.close();
				} catch (Exception e) {
					Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
				}
			}
		}
	}

	public static void rollbackConnection(Connection con) {
		if (con != null) {
			try {
				con.rollback();
			} catch (SQLException e) {
				Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
			}
		}
	}

	public static void initDB() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			con = DBUtils.getConnection();
			
			con.setAutoCommit(false);
			
			String[] sentencias = {
					"DROP TABLE IF EXISTS pictures;",
					"DROP TABLE IF EXISTS boxes",
					"DROP TABLE IF EXISTS users",
					"DROP TABLE IF EXISTS business",
					"DROP SEQUENCE IF EXISTS pictures_seq",
					"DROP SEQUENCE IF EXISTS business_seq",
					"DROP SEQUENCE IF EXISTS users_seq",
					"DROP SEQUENCE IF EXISTS boxes_seq",
					"CREATE TABLE business (id int PRIMARY KEY, name text NOT NULL)",
					"CREATE TABLE users (identification varchar(15) PRIMARY KEY, encrypted_pass text NOT NULL, name text NOT NULL, id_company int NOT NULL REFERENCES business(id), valid boolean NOT NULL DEFAULT false)",
					"CREATE TABLE boxes (uuid varchar(15) PRIMARY KEY, code_qr bytea, id_company int NOT NULL REFERENCES business(id))",
					"CREATE TABLE pictures (id int PRIMARY KEY, name text NOT NULL, uuid_box varchar(15) NOT NULL REFERENCES boxes(uuid), image bytea)",
					"CREATE SEQUENCE business_seq", 
					"CREATE SEQUENCE pictures_seq",
					"INSERT INTO business VALUES (nextval('business_seq'), 'Art Transportation')",
					"INSERT INTO business VALUES (nextval('business_seq'), 'Escola del Treball de Barcelona')",
					"INSERT INTO users VALUES ('111', '"+Validation.generateHashSha256("111")+"', 'Administrador', 1, true)",
					"INSERT INTO users VALUES ('222', '"+Validation.generateHashSha256("222")+"', 'Laura', 2, true)",
					};
			
			for (String sentencia: sentencias) {
				ps = con.prepareStatement(sentencia);
				ps.executeUpdate();
			}
			
			con.commit();

		} catch (SQLException e) {
			if (con != null) rollbackConnection(con);
			Logger.getLogger(DBUtils.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}
	}

}
