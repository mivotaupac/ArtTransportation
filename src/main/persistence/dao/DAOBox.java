package main.persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.entities.Box;
import main.persistence.DBUtils;

public class DAOBox {

	// SELECT's

	public byte[] getImageQR(String uuid) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] fileBox = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT code_qr FROM boxes WHERE uuid = ?");

			ps.setString(1, uuid);

			rs = ps.executeQuery();

			if (rs.next()) {
				fileBox = rs.getBytes("code_qr");
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOBox.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return fileBox;
	}

	public Box getBoxByUuid(String uuid) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Box box = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT id_company FROM boxes WHERE uuid = ?");

			rs = ps.executeQuery();

			if (rs.next()) {
				box = new Box();
				box.setUuid(uuid);
				box.setIdCompany(rs.getInt("id_company"));
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOBox.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return box;
	}

	public List<Box> getBoxesByCompany(int idCompany) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Box> boxes = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT uuid FROM boxes WHERE id_company = ?");

			ps.setInt(1, idCompany);

			rs = ps.executeQuery();

			boxes = new ArrayList<Box>();
			Box box;
			while (rs.next()) {
				box = new Box();
				box.setUuid(rs.getString("uuid"));
				box.setIdCompany(idCompany);
				boxes.add(box);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOBox.class.getName()).log(Level.SEVERE, null, e);
			return null;
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return boxes;
	}

	public List<Box> getAllBoxes() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Box> boxes = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT uuid, id_company FROM boxes");

			rs = ps.executeQuery();

			boxes = new ArrayList<Box>();
			Box box;
			while (rs.next()) {
				box = new Box();
				box.setUuid(rs.getString("uuid"));
				box.setIdCompany(rs.getInt("id_company"));
				boxes.add(box);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOBox.class.getName()).log(Level.SEVERE, null, e);
			return null;
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return boxes;
	}

	// INSERT'S

	public boolean insertBox(Box box, byte[] codeQR) {

		Connection con = null;
		PreparedStatement ps = null;
		int result = 0;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("INSERT INTO boxes VALUES (?, ?, ?)");

			ps.setString(1, box.getUuid());
			ps.setBytes(2, codeQR);
			ps.setInt(3, box.getIdCompany());

			result = ps.executeUpdate();

		} catch (SQLException e) {
			Logger.getLogger(DAOBox.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(ps, con);
		}

		return result != 0;
	}

}
