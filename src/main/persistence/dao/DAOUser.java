package main.persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.entities.User;
import main.persistence.DBUtils;

public class DAOUser {

	// SELECT's

	public String getBusinessName(int id) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String name = null;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement("SELECT name FROM business WHERE id = ?");

			ps.setInt(1, id);

			rs = ps.executeQuery();

			if (rs.next()) {
				name = rs.getString("name");
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return name;
	}

	public User getUserById(String identification) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User user = null;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement("SELECT name, id_company, valid FROM users WHERE identification = ?");

			ps.setString(1, identification);

			rs = ps.executeQuery();

			if (rs.next()) {
				user = new User();
				user.setIdentification(identification);
				user.setName(rs.getString("name"));
				user.setIdCompany(rs.getInt("id_company"));
				user.setValid(rs.getBoolean("valid"));
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return user;
	}

	public boolean validatePass(String identification, String encryptedPassword) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		boolean result = false;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement("SELECT encrypted_pass = ? FROM users WHERE identification = ?");

			ps.setString(1, encryptedPassword);
			ps.setString(2, identification);

			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getBoolean(1);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return result;
	}

	public List<User> getUsersByCompany(int idCompany) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<User> users = null;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement("SELECT identification, name, valid FROM users WHERE id_company = ?");

			ps.setInt(1, idCompany);

			rs = ps.executeQuery();

			users = new ArrayList<User>();
			User user;
			while (rs.next()) {
				user = new User();
				user.setIdentification(rs.getString("identification"));
				user.setName(rs.getString("name"));
				user.setIdCompany(idCompany);
				user.setValid(rs.getBoolean("valid"));
				users.add(user);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
			return null;
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return users;
	}

	public List<User> getAllUsers() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<User> users = null;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement("SELECT identification, name, id_company, valid FROM users");

			rs = ps.executeQuery();

			users = new ArrayList<User>();
			User user;
			while (rs.next()) {
				user = new User();
				user.setIdentification(rs.getString("identification"));
				user.setName(rs.getString("name"));
				user.setIdCompany(rs.getInt("id_company"));
				user.setValid(rs.getBoolean("valid"));
				users.add(user);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
			return null;
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return users;
	}

	// INSERT'S

	public boolean insertUser(User user, String encryptedPassword) {

		Connection con = null;
		PreparedStatement ps = null;
		int result = 0;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement("INSERT INTO users VALUES (?, ?, ?, ?, ?)");

			ps.setString(1, user.getIdentification());
			ps.setString(2, encryptedPassword);
			ps.setString(3, user.getName());
			ps.setInt(4, user.getIdCompany());
			ps.setBoolean(5, user.isValid());

			result = ps.executeUpdate();

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(ps, con);
		}

		return result != 0;
	}

	// UPDATE'S

	public boolean updateUserPassword(User user, String oldEncryptedPassword, String newEncryptedPassword) {

		Connection con = null;
		PreparedStatement ps = null;
		int result = 0;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement(
					"UPDATE users SET encrypted_pass = ? WHERE identification = ? AND encrypted_pass = ?");

			ps.setString(1, newEncryptedPassword);
			ps.setString(2, user.getIdentification());
			ps.setString(3, oldEncryptedPassword);

			result = ps.executeUpdate();

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(ps, con);
		}

		return result != 0;
	}

	public boolean updateUserInfo(User user) {

		Connection con = null;
		PreparedStatement ps = null;
		int result = 0;

		try {

			con = DBUtils.getConnection();
			ps = con.prepareStatement("UPDATE users SET name = ?, id_company = ?, valid = ? WHERE identification = ?");

			ps.setString(1, user.getName());
			ps.setInt(2, user.getIdCompany());
			ps.setBoolean(3, user.isValid());
			ps.setString(4, user.getIdentification());

			result = ps.executeUpdate();

		} catch (SQLException e) {
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(ps, con);
		}

		return result != 0;
	}

	public boolean updateStatusUsers(List<User> users, boolean status) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;

		try {

			con = DBUtils.getConnection();
			con.setAutoCommit(false);

			ps = con.prepareStatement("UPDATE users SET valid = ? WHERE identification = ?");

			for (User user : users) {
				ps.setBoolean(1, status);
				ps.setString(2, user.getIdentification());
				ps.addBatch();
			}

			result = ps.executeBatch().length == users.size();

			con.commit();

		} catch (SQLException e) {
			result = false; // por si falla en el commit
			DBUtils.rollbackConnection(con);
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(ps, con);
		}

		return result;
	}

	// DELETE'S

	public boolean deleteUsers(List<User> users) {

		Connection con = null;
		PreparedStatement ps = null;
		boolean result = false;

		try {

			con = DBUtils.getConnection();
			con.setAutoCommit(false);

			ps = con.prepareStatement("DELETE FROM users WHERE identification = ?");

			for (User user : users) {
				ps.setString(1, user.getIdentification());
				ps.addBatch();
			}

			result = ps.executeBatch().length == users.size();

			con.commit();

		} catch (SQLException e) {
			result = false; // por si falla en el commit
			DBUtils.rollbackConnection(con);
			Logger.getLogger(DAOUser.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(ps, con);
		}

		return result;
	}

}
