package main.persistence.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.entities.Picture;
import main.persistence.DBUtils;

public class DAOPicture {

	// SELECT's

	public int getNextSequence() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int nextVal = -1;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT nextval('pictures_seq')");

			rs = ps.executeQuery();

			if (rs.next()) {
				nextVal = rs.getInt(1);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOBox.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return nextVal;
	}

	public byte[] getImagePicture(int idPicture) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		byte[] filePicture = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT image FROM pictures WHERE id = ?");

			ps.setInt(1, idPicture);

			rs = ps.executeQuery();

			if (rs.next()) {
				filePicture = rs.getBytes("image");
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOPicture.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return filePicture;
	}

	public List<Picture> getPicturesByBox(String uuid) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Picture> pictures = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT id, name FROM pictures WHERE uuid_box = ?");

			ps.setString(1, uuid);

			rs = ps.executeQuery();

			pictures = new ArrayList<Picture>();
			Picture picture;
			while (rs.next()) {
				picture = new Picture();
				picture.setId(rs.getInt("id"));
				picture.setName(rs.getString("name"));
				picture.setUuidBox(uuid);
				pictures.add(picture);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOPicture.class.getName()).log(Level.SEVERE, null, e);
			return null;
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return pictures;
	}

	public List<Picture> getPicturesByCompany(String company) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Picture> pictures = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement(
					"SELECT id, name, uuid_box FROM pictures WHERE uuid_box IN (SELECT uuid FROM boxes WHERE company = ?)");

			ps.setString(1, company);

			rs = ps.executeQuery();

			pictures = new ArrayList<Picture>();
			Picture picture;
			while (rs.next()) {
				picture = new Picture();
				picture.setId(rs.getInt("id"));
				picture.setName(rs.getString("name"));
				picture.setUuidBox(rs.getString("uuid_box"));
				pictures.add(picture);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOPicture.class.getName()).log(Level.SEVERE, null, e);
			return null;
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return pictures;
	}

	public List<Picture> getAllPictures() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<Picture> pictures = null;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("SELECT id, name, uuid_box FROM pictures");

			rs = ps.executeQuery();

			pictures = new ArrayList<Picture>();
			Picture picture;
			while (rs.next()) {
				picture = new Picture();
				picture.setId(rs.getInt("id"));
				picture.setName(rs.getString("name"));
				picture.setUuidBox(rs.getString("uuid_box"));
				pictures.add(picture);
			}

		} catch (SQLException e) {
			Logger.getLogger(DAOPicture.class.getName()).log(Level.SEVERE, null, e);
			return null;
		} finally {
			DBUtils.closeResources(rs, ps, con);
		}

		return pictures;
	}

	// INSERT'S

	public boolean insertPicture(Picture picture, byte[] image) {

		Connection con = null;
		PreparedStatement ps = null;
		int result = 0;

		try {

			con = DBUtils.getConnection();

			ps = con.prepareStatement("INSERT INTO pictures VALUES (?, ?, ?, ?)");

			ps.setInt(1, picture.getId());
			ps.setString(2, picture.getName());
			ps.setString(3, picture.getUuidBox());
			ps.setBytes(4, image);

			result = ps.executeUpdate();

		} catch (SQLException e) {
			Logger.getLogger(DAOPicture.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			DBUtils.closeResources(ps, con);
		}

		return result != 0;
	}

}
